/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minesweeper;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author JosepB
 */
public class Cell {
   
    //ATTRIBUTES de una casilla.
    private boolean mine;
    private int near_mines;

    private boolean view;

    private int i, j;
    
    //ATTRIBUTES de UI.
    private Rectangle2D.Float rec;

    //BUILDER
    public Cell(){
        
    }
    public Cell(int i, int j, Rectangle2D.Float rec) {
        this.view = false;
        this.mine = false;
        this.near_mines = -1;

        this.i = i;
        this.j = j;
        
        this.rec = rec;
    }

    /*
        Métodes per posar el nombre de mines que hi ha al voltans y 
        saber cuantas minas tiene alrededor.
     */
    public int GetNumMines() {
        return this.near_mines;
    }

    public void SetNumMines(int nm) {
        this.near_mines = nm;
    }

    /*
        Método para saber si la casilla ha sido visitada
     */
    public boolean IsSeeing() {
        return this.view;
    }

    /*
        Métodos para tapar y destapar las casillas.
     */
    public void SeeCell() {
        this.view = true;
    }

    public void NoSeeCell() {
        this.view = false;
    }

    /*
        Nos permite poner una mina a la casilla
     */
    public void PutMine() {
        this.mine = true;
    }

    /*
    Devuelve:
        TRUE - si la casilla tiene mina.
        FALSE - si la casilla no tiene mina.
     */
    public boolean HasMine() {
        return this.mine;
    }

    // Get de i y j
    public int Geti() {
        return this.i;
    }

    public int Getj() {
        return this.j;
    }

    public void paintComponent (Graphics g) throws IOException{
        if(this.view || this.near_mines >= 0){
            if(this.mine){
                g.drawImage(ImageIO.read(new File("IMAGES/mine.png")),(int) this.rec.x, (int) this.rec.y, null);
            }else{
                int mod = (this.i+this.j)%2;
                g.drawImage(ImageIO.read(new File("IMAGES/"+this.near_mines+"_"+mod+".png")), (int) this.rec.x, (int) this.rec.y, null);
            }
        }else{
            int mod = (this.i+this.j)%2;
            g.drawImage(ImageIO.read(new File("IMAGES/no_mine_"+mod+".png")), (int) this.rec.x, (int) this.rec.y, null);
        }
    }
    
    //Método toString para imprimir la casilla.
    @Override
    public String toString() {
        String aux = "";
        if (this.view || this.near_mines >= 0) {
            if (this.mine) {
                aux = "X";
            } else {
                aux += this.near_mines;
            }
        } else {
            aux = "-";
        }
        return aux;
    }

}
