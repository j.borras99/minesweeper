package minesweeper;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

/**
 *
 * @author Josep Borràs Sánchez
 */
public class Minesweeper extends JFrame implements MouseListener {

    //ATTRIBUTES to play a game
    private Board board;
    //Al principio mostrarà el tablero de 9x9. En formato Principiante.
    private int n = 9;
    private int num_mines = 10;

    //ATTRIBUTES to UI
    private JMenuBar barra_menu;
    private JMenu menu;
    private JMenuItem jsalir;
    private JMenuItem jreiniciar;

    public static void main(String[] args) {
        //(new Minesweeper()).inicio(); // LINEA DE COMANDES
        new Minesweeper().setVisible(true); // UI
    }

    // BUILDER
    public Minesweeper() {
        this.setSize((Board.SIDE * n), (Board.SIDE * n) + 44);
        this.setTitle("Mines Weeper");
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(Minesweeper.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.initComponents();
    }

    /*
        Inicialitzación de los componentes
        Crea el menú, el tablero, lo inicializa por primera vez y añade
        los components en el JFrame.
     */
    private void initComponents() {

        this.board = new Board(this.n, this.num_mines);
        // Inicialización del MENÚ
        this.barra_menu = new JMenuBar();
        this.menu = new JMenu();
        this.jsalir = new JMenuItem();
        this.jreiniciar = new JMenuItem();

        this.getContentPane().add(this.barra_menu);

        this.jreiniciar.setText("Reiniciar");
        this.jreiniciar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                jreiniciarActionPerformed(evt);
            }
        });

        this.jsalir.setText("Salir");
        this.jsalir.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                jsalirActionPerformed(evt);
            }
        });

        this.menu.setText("Juego");
        this.menu.add(this.jreiniciar);
        this.menu.add(this.jsalir);

        this.barra_menu.add(this.menu);
        this.setJMenuBar(this.barra_menu);

        //Inicialización del Tableroç
        this.board.initBoard();
        this.board.addMouseListener(this);
        this.getContentPane().add(this.board);
    }

    /*
        Métodos que se ejecutaran al pulsar las opciones del menu.
     */
    private void jreiniciarActionPerformed(ActionEvent evt) {
        this.board.initBoard();
        repaint();
    }

    private void jsalirActionPerformed(ActionEvent evt) {
        System.exit(0);
    }

    /*
        Métodos encargados de controlar la interacción con el mouse.
        Después de cada pulsación comprovará si el juego ha terminado y si ha sido
        ganador o no.
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        int x, y;
        Cell c;

        if (e.getButton() == MouseEvent.BUTTON1) {
            try {
                x = e.getX();
                y = e.getY();
                c = this.board.getCasella(x, y);
                this.board.SelectCell(c.Geti(), c.Getj());
                repaint();
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                Logger.getLogger(Minesweeper.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (this.board.Win() || this.board.Explote()) {
                if (this.board.Explote()) {
                    JOptionPane.showMessageDialog(null, "Ups! Ha habido una explosion", "Otra vez sera!!", JOptionPane.PLAIN_MESSAGE, null);
                } else {
                    JOptionPane.showMessageDialog(null, "Enhorabuena!! No has encontrado ninguna mina", "ENHORABUENA!", JOptionPane.PLAIN_MESSAGE, null);
                }
                this.board.initBoard();
                repaint();
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    /*
        Método para redimensionar la ventana que tenemos abierta y añadir el nuevo tablero
     */
    private void resizeBoard() {
        this.setBounds(0, 0, (n * Board.SIDE) + 5, (n * Board.SIDE) + 44);
        this.getContentPane().remove(this.board);
        this.board = new Board(this.n, this.num_mines);
        this.board.addMouseListener(this);
        this.getContentPane().add(this.board);
    }

    /*
        BUSCAMINAS NO UI
     */
    public void inicio() {
        int n, nm, x, y;

        System.out.print("Dimension del tablero: ");
        n = this.readInt();
        System.out.print("Numero de minas: ");
        nm = this.readInt();
        while (nm > n * n) {
            System.out.println("No puede haver mas minas que casillas");
            System.out.print("Numero de minas: ");
            nm = this.readInt();
        }
        System.out.println("Que empieze el juego, mucha suerte !!");
        board = new Board(n, nm);
        System.out.println(board);

        board.Destapar();
        System.out.println(board);
        board.Tapar();

        System.out.println(board);
        while (!board.Win() && !board.Explote()) {
            System.out.print("X = ");
            x = readInt();
            System.out.print("Y = ");
            y = readInt();
            board.SelectCell(x, y);
            System.out.println(board);

        }
        if (board.Win()) {
            System.out.println("ENHORABUENA!!");
        } else {
            System.out.println("UUPS! Ha habido una explosion");
        }

    }

    /*
        Método para leer un número de teclado
     */
    private int readInt() {
        String aux = "";
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            aux = br.readLine();
        } catch (IOException ioe) {
            ioe.getMessage();
        }
        return Integer.parseInt(aux);
    }

}
