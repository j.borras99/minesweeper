/*
    La clase tablero estarà formada por una array bidimensional de casillas
    y de una dimensions n. El tablero serà cuadrado de n x n.
 */
package minesweeper;

import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.util.Random;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;

/**
 *
 * @author Josep Borràs Sánchez
 */
public class Board extends JPanel {

    //ATTRIBUTES de funcionamiento.
    private Cell[][] board;
    private int dim;
    private int num_mines;

    private boolean isExploted;

    private Stack<Cell> stack = new Stack<Cell>();

    //ATTRIBUTES de UI
    public final static int SIDE = 30;
    private int max_dim;

    //BUILDER
    public Board(int n, int nm) {
        this.dim = n;
        this.max_dim = this.dim * SIDE;
        this.num_mines = nm;
        board = new Cell[n][n];
        for(int i=0;i<this.dim;i++){
            for(int j=0; j<this.dim;j++){
                this.board[i][j] = new Cell();
            }
        }
    }

    /*
        Método para evaluar la casilla para ver cuantas minas hay alrededor.
     */
    private void avaluar(int x, int y) {
        int nm = 0;

        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                if (i >= 0 && j >= 0 && i < this.dim && j < this.dim && !(i == x && j == y)) {
                    if (this.board[i][j].HasMine()) {
                        nm++;
                    }
                }
            }
        }
        this.board[x][y].SetNumMines(nm);
    }

    /*
        Método para destapar una casilla. Actualizarà el atributo isExploted en 
        caso de que la casilla destapada tenga una bomba y explote.
     */
    public void SelectCell(int x, int y) {
        if (this.board[x][y].HasMine()) {
            this.board[x][y].SeeCell();
            this.isExploted = true;
        } else {
            avaluar(x, y);
            this.board[x][y].SeeCell();
            if (this.board[x][y].GetNumMines() == 0) {
                //No tiene minas alrededor, por lo tanto se miran las casillas de alrededor.
                for (int i = x - 1; i <= x + 1; i++) {
                    for (int j = y - 1; j <= y + 1; j++) {
                        if (i >= 0 && j >= 0 && i < this.dim && j < this.dim && !(i == x && j == y)) {
                            avaluar(i, j);
                            if (this.board[i][j].GetNumMines() == 0) {
                                if (!stack.contains(this.board[i][i]) && !this.board[i][j].IsSeeing()) {
                                    stack.push(this.board[i][j]);
                                }
                            } else {
                                this.board[i][j].SeeCell();
                            }
                        }
                    }
                }
                AvaluarPila();
            }
        }
    }

    /*
        Método que se llama después de haber seleccionado una casilla. Este vacia
        la pila y evalua las casillas que hay dentro de la pila.
     */
    private void AvaluarPila() {
        int x, y;

        while (!stack.empty()) {
            Cell c = stack.pop();
            x = c.Geti();
            y = c.Getj();

            this.board[x][y].SeeCell();
            for (int i = x - 1; i <= x + 1; i++) {
                for (int j = y - 1; j <= y + 1; j++) {
                    if (i >= 0 && j >= 0 && i < this.dim && j < this.dim && !(i == x && j == y)) {
                        avaluar(i, j);
                        if (this.board[i][j].GetNumMines() == 0) {
                            if (!stack.contains(this.board[i][i]) && !this.board[i][j].IsSeeing()) {
                                stack.push(this.board[i][j]);
                            }
                        } else {
                            this.board[i][j].SeeCell();
                        }
                    }
                }
            }
        }
    }

    /*
        Método que nos dice si ha habido explosión o no.
     */
    public boolean Explote() {
        return this.isExploted;
    }

    /*
        Método que nos va a mirar si la partida esta ganada.
        Una partida esta ganada si:
            - No ha explotado ninguna mina
            - Todas las casillas que no tienen mina estan visibles.
     */
    public boolean Win() {
        boolean win = true;
        int i = 0;
        int j = 0;

        while (i < this.dim && win) {
            if (!this.board[i][j].HasMine()) {
                if (!this.board[i][j].IsSeeing()) {
                    win = false;
                }
            }

            if (j + 1 == this.dim) {
                j = 0;
                i++;
            } else {
                j++;
            }
        }
        return win;
    }

    /*
        Método para destapar el tablero y taparlo de nuevo.
     */
    public void Destapar() {
        for (int i = 0; i < this.dim; i++) {
            for (int j = 0; j < this.dim; j++) {
                this.board[i][j].SetNumMines(0);
            }
        }
    }

    public void Tapar() {
        for (int i = 0; i < this.dim; i++) {
            for (int j = 0; j < this.dim; j++) {
                this.board[i][j].SetNumMines(-1);
            }
        }
    }

    /*
        Inicializa el tablero con el numero de minas requeridas 
        y en su respectivo lugar (aleatorio)
     */
    public void initBoard() {
        this.isExploted = false;
        
        Random ran = new Random();
        int mines_put = 0;
        int mines_pos_x = 0;
        int mines_pos_y = 0;
        int y = 0;

        for (int i = 0; i < this.dim; i++) {
            int x = 0;
            for (int j = 0; j < this.dim; j++) {
                Rectangle2D.Float rec = new Rectangle2D.Float(x, y, this.SIDE, this.SIDE);
                this.board[i][j] = new Cell(i, j, rec);
                x += this.SIDE;
            }
            y += this.SIDE;
        }

        while (mines_put != this.num_mines) {
            mines_pos_x = ran.nextInt(this.dim);
            mines_pos_y = ran.nextInt(this.dim);
            if (!board[mines_pos_x][mines_pos_y].HasMine()) {
                board[mines_pos_x][mines_pos_y].PutMine();
                mines_put++;
            }
        }
    }

    @Override
    public void paint(Graphics g) {
        for (int i = 0; i < this.dim; i++) {
            for (int j = 0; j < this.dim; j++) {
                try {
                    this.board[i][j].paintComponent(g);
                } catch (IOException ex) {
                    ex.getMessage();
                }
            }
        }
    }

    //Método toString para imprimir el tablero.
    @Override
    public String toString() {
        String aux = "";
        for (int i = 0; i < this.dim; i++) {
            for (int j = 0; j < this.dim; j++) {
                aux += this.board[i][j] + " ";
            }
            aux += "\n";
        }
        return aux;
    }

    /*
        Método para obtener la casilla en la que se ha pulsado con el mouse.
    */
    public Cell getCasella(int x, int y) { 
        int i = y / SIDE;
        int j = x / SIDE;
        return this.board[i][j];
    }
}
